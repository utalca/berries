﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadFruit : Product
{
    public override ProductType Type
    {
        get
        {
            return Product.ProductType.ORGANIC;
        }
    }

    protected override GameObject FindModel()
    {
        GameObject[] database = Resources.LoadAll<GameObject>(string.Format("Products/{0}/{1}", typeof(BadFruit).Name, GameManager.Singleton.fruitType.ToString()));

        return database[Random.Range(0, database.Length - 1)];
    }
}
