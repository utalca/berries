﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inorganic : Product
{
    public bool isMetal = false;

    public override ProductType Type
    {
        get
        {
            return Product.ProductType.INORGANIC;
        }
    }

    protected override GameObject FindModel()
    {
        string type = "";

        if (isMetal)
            type = "Metal";
        else
            type = "NoMetal";

        GameObject[] database = Resources.LoadAll<GameObject>(string.Format("Products/{0}/{1}", typeof(Inorganic).Name, type));

        return database[Random.Range(0, database.Length - 1)];
    }
}
