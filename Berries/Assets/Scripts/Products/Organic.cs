﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Organic : Product
{

    public override ProductType Type
    {
        get
        {
            return Product.ProductType.ORGANIC;
        }
    }

    protected override GameObject FindModel()
    {
        GameObject[] database = Resources.LoadAll<GameObject>(string.Format("Products/{0}/{1}", typeof(Organic).Name, "Nature"));

        return database[Random.Range(0, database.Length - 1)];
    }
}
