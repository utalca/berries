﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Product : MonoBehaviour {

    private bool isInit = false;
    protected GameObject model;
    private Rigidbody _rb;

    public enum ProductType
    {
        FRUIT, ORGANIC, INORGANIC, BAD_FRUIT
    }

    protected float _quality = 0;

    public Rigidbody Rb
    {
        get
        {
            return _rb;
        }
    }

    public float Quality
    {
        get
        {
            return _quality;
        }
    }

    public abstract ProductType Type
    {
        get;
    }
    

    private void OnEnable()
    {
        Init();
    }

    public void Init()
    {
        if (!isInit)
        {
            model = Instantiate(FindModel(), transform);
            _rb = GetComponentInChildren<Rigidbody>();
            isInit = true;
            model.tag = tag;
        }
    }

    public void Select()
    {
        Rb.isKinematic = true;
    }

    public void Deselect()
    {
        Rb.isKinematic = false;
    }

    protected abstract GameObject FindModel();



}
