﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : Product
{
    private Rigidbody _rg;

    public override ProductType Type
    {
        get
        {
            return Product.ProductType.FRUIT;
        }
    }

    protected override GameObject FindModel()
    {
        GameObject[] database = Resources.LoadAll<GameObject>(string.Format("Products/{0}/{1}", typeof(Fruit).Name, GameManager.Singleton.fruitType.ToString()));
        return database[Random.Range(0, database.Length - 1)];
    }

    private void SetQualiy()
    {
        if (Random.Range(0f,1f) < GameManager.Singleton.QualityRate)
        {
            float quality = Random.Range(GameManager.Singleton.QualityRate, 1);
            transform.localScale = Vector3.one * quality;
            Color qualityColor = (Color.white * (1-quality));
            transform.GetChild(0).GetComponent<MeshRenderer>().material.color = MixColors(new Color[] { Color.red, qualityColor });
            _quality = quality;
        }
        else
        {
            float quality = Random.Range(0, GameManager.Singleton.QualityRate);
            float size = Mathf.Clamp(quality, 0.4f, 1f);
            transform.localScale = Vector3.one * size;
            Color qualityColor = (Color.white * quality * 2);
            transform.GetChild(0).GetComponent<MeshRenderer>().material.color = MixColors(new Color[] { Color.red, qualityColor});
            _quality = quality;
        }
    }

    void Start()
    {
        _rg = GetComponent<Rigidbody>();
        SetQualiy();
    }

    private Color MixColors(Color[] colors)
    {
        Color aux = new Color();

        foreach (Color c in colors)
        {
            aux += c;
        }

        aux /= colors.Length;

        return aux;
    }
}
