﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SimulationOptions : MonoBehaviour {

    public Button[] dificult;
    public Sprite normal;
    public Sprite selected;
    public float[] values;
    public int table;
    
    public void PressedBtn(int btn)
    {
        for (int i = 0; i < dificult.Length; i++)
        {
            dificult[i].image.sprite = normal;
        }
        dificult[btn].image.sprite = selected;
        EventSystem myEventSystem = EventSystem.current;// = GameObject.Find("EventSystem");
        myEventSystem.SetSelectedGameObject(null);

        PlayManagement.Singleton.GetValue(table, values[btn]);
    }

    public void PlayBtn2(string scene)
    {
        SceneManager.LoadScene("Scenes/" + scene);
    }
}
