﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayManagement : MonoBehaviour {

    public Button play;

    bool[] pressed = new bool[4];

    private static PlayManagement _singleton;

    public static PlayManagement Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = GameObject.FindObjectOfType<PlayManagement>();

            return _singleton;
        }
    }

    public void GetValue(int table, float value)
    {
        pressed[table] = true;

        switch (table)
        {
            case 0: GameManager.Singleton.BeltSpeed = value; break;
            case 1: GameManager.Singleton.CreationRate = value; break;
            case 2: GameManager.Singleton.ProductCapacity = value; break;
            case 3: GameManager.Singleton.QualityRate = value; break;
        }

        for (int i = 0; i < pressed.Length; i++)
        {
            if (!pressed[i])
                return;
        }

        play.interactable = true;
    }
}
