﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject SimulationOptionMenu;
    public GameObject MainOptionsMenu;

    // Use this for initialization
    void Start () {
        SimulationOptionMenu.SetActive(false);
        MainOptionsMenu.SetActive(true);
    }

    public void StartOptionSimulation()
    {
        SimulationOptionMenu.SetActive(true);
        MainOptionsMenu.SetActive(false);
    }

    public void BackBtn()
    {
        SimulationOptionMenu.SetActive(false);
        MainOptionsMenu.SetActive(true);
    }

    public void PlayBtn(string scene)
    {
        SceneManager.LoadScene("Scenes/" + scene);
    }

}
