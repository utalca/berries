﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager  {

    private static GameManager _singleton;

    private float _beltSpeed = 0.2f;
    private float _creationRate = 0.3f;
    private float _qualityRate = 0.5f;
    private float _productCapacity = 0.1f;

    public FruitType fruitType = FruitType.RASPBERRY;
    private List<StoredProducts> lastResults;

    public static GameManager Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = new GameManager();

            return _singleton;
        }
    }

    public float BeltSpeed
    {
        get
        {
            return _beltSpeed;
        }

        set
        {
            _beltSpeed = Mathf.Clamp01(value);
        }
    }

    public float CreationRate
    {
        get
        {
            return _creationRate;
        }

        set
        {
            _creationRate = Mathf.Clamp01(value);
        }
    }

    public float QualityRate
    {
        get
        {
            return _qualityRate;
        }

        set
        {
            _qualityRate = Mathf.Clamp01(value);
        }
    }

    public float ProductCapacity
    {
        get
        {
            return _productCapacity;
        }

        set
        {
            _productCapacity = Mathf.Clamp01(value);
        }
    }

    public List<StoredProducts> LastResults
    {
        get
        {
            return lastResults;
        }
    }

    public void ShowResults(List<StoredProducts> storage)
    {
        lastResults = new List<StoredProducts>(storage);

        UnityEngine.SceneManagement.SceneManager.LoadScene("Results");
    }
}

public enum FruitType
{
    RASPBERRY
}
