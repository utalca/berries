﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SorterMachine : MonoBehaviour {

    public float area = 0;
    public GameObject fruitPrefab;
    public GameObject badFruitPrefab;
    public GameObject inorganicMetalPrefab;
    public GameObject inorganicPrefab;
    public GameObject organicPrefab;
    // GameObject clone;
    int countProducts = 0;
    public int limitProducts = 5;
    private IEnumerator coroutine;
    List<GameObject> products;
    private static SorterMachine _singleton;

    private bool _isActive = true;

    GameObject trash;
    int x = 0;

    public static SorterMachine Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = Object.FindObjectOfType<SorterMachine>();

            return _singleton;
        }
    }

    public bool IsActive
    {
        get
        {
            return _isActive;
        }

        set
        {
            if (value)
                StartCoroutine(Spawn());

            _isActive = value;
        }
    }

    

    void Start()
    {
        products = new List<GameObject>();
        float creationRate = GameManager.Singleton.CreationRate; //tiempo que se activan los productos
        float qualityRate = GameManager.Singleton.QualityRate;

        CreatePool(qualityRate);

        StartCoroutine(Spawn());
    }

    void CreatePool(float quality)
    {
        float fruit = limitProducts * quality;

        for (int i = 0; i < fruit; i++)
        {
            CreateProduct(fruitPrefab);
        }

        float bad = (limitProducts - fruit) * 0.5f;

        for (int i = 0; i < bad; i++)
        {
            CreateProduct(badFruitPrefab);
        }

        float other = limitProducts - (fruit + bad);


        for (int i = 0; i < other; i++)
        {
            int option  = Random.Range(1,4);
                
            switch (option)
            {
                case 1:
                    CreateProduct(inorganicMetalPrefab);
                    break;
                case 2:
                    CreateProduct(inorganicPrefab);
                    break;
                case 3:
                    CreateProduct(organicPrefab);
                    break;
                default:
                    break;
            }
        }
    }

    private void CreateProduct(GameObject product)
    {
        Vector3 position = GeneratePosition();
        var clone = Instantiate(product, position, Random.rotation);
        clone.SetActive(false);
        products.Add(clone);
        countProducts++;
    }

    private Vector3 GeneratePosition()
    {
        float ran1, ran2;

        ran1 = Random.Range(transform.position.x - (area / 2), transform.position.x + (area / 2));
        ran2 = Random.Range(transform.position.z - (area / 2), transform.position.z + (area / 2));

        Vector3 position = new Vector3(ran1, transform.position.y, ran2);

        return position;
    }

    private IEnumerator Spawn()
    {
        while (IsActive)
        {
            int randIndex = Random.Range(0, limitProducts - 1);

            ActivateProduct(randIndex);

            yield return new WaitForSeconds( 1 - GameManager.Singleton.CreationRate);
        }
    }

    void ActivateProduct(int indexProduct)
    {
        if (!products[indexProduct].activeSelf)
        {
            products[indexProduct].transform.position = GeneratePosition();
            products[indexProduct].SetActive(true);
        }
    }
   
    public void TurnOffAll() 
    {
        for (int i = 0; i < products.Count; i++)
        {
            products[i].SetActive(false);
        }
        IsActive = false;
   }
   /*
  void initializedProduct(GameObject prefab)
  {
      float ran1, ran2;

      ran1 = Random.Range(transform.position.x - (area / 2), transform.position.x + (area / 2));
      ran2 = Random.Range(transform.position.z - (area / 2), transform.position.z + (area / 2));

      Vector3 posicion = new Vector3(ran1, transform.position.y, ran2);
      prefab.transform.position = posicion;
  }
  */
            private void OnDrawGizmos() //funcion que ejecuta cuando se ha actualizado una escena o esta cambia. dibujando lineas de debug
    {
        Gizmos.color = Color.cyan; //pintar una caja para que lo veamos como desarrollador. el jugador no lo verá    
        Vector3 size = new Vector3(area, 1, area);
        //Gizmos.DrawCube(transform.position, size);
        Gizmos.DrawWireCube(transform.position, size);
    }

    private void OnGUI()
    {
        int hiden = 0, visible = 0;

        foreach (GameObject go in products)
        {
            if (go.activeSelf)
                visible++;
            else
                hiden++;
        }

        GUI.Box(new Rect(0, 0, 150, 100), "Total products: " + products.Count);
        GUI.Box(new Rect(0, 120, 150, 100), "Visible products: " + visible);
        GUI.Box(new Rect(0, 240, 150, 100), "Hidden products: " + hiden);
    }

}
