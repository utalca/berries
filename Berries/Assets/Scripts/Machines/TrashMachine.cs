﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashMachine : MonoBehaviour {

    public int trashLimit = 20;

    private List<GameObject> trashContent = new List<GameObject>();

    public Collider trigger;

    private void SortProduct(GameObject product)
    {
        if (!trashContent.Contains(product))
            trashContent.Add(product);

        if (trashContent.Count >= trashLimit)
            Clear();
    }

    private void Clear()
    {
        foreach (GameObject go in trashContent)
        {
            go.SetActive(false);
        }

        trashContent.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        SortProduct(other.transform.root.gameObject);
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(0, 360, 150, 100), "Trash products: " + trashContent.Count);
    }
}
