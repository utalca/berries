﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltMachine : MonoBehaviour
{

    float speed = 15f;
    public bool on = true;
    Vector2 offset = new Vector2(0f, 0f);

    Renderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<MeshRenderer>();

        speed *= GameManager.Singleton.BeltSpeed;
    }

    void OnCollisionStay(Collision obj)
    {
        if (obj.collider.tag.Equals("Product"))
        {
            Rigidbody rb = obj.gameObject.GetComponent<Rigidbody>();
            float beltVelocity = speed * Time.deltaTime * 10;
            rb.velocity = beltVelocity * transform.parent.forward;
        }
    }

    void Update()
    {
        offset -= new Vector2(0, 0.1f) * Time.deltaTime * speed;
        _renderer.material.SetTextureOffset("_MainTex", offset);
    }
}