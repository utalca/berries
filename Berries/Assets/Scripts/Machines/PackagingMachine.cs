﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagingMachine : MonoBehaviour {

    public List<StoredProducts> storage;
    public float storageQuality;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Product"))
        {
            StoreProduct(other.transform.root.GetComponent<Product>());
        }
    }

    private void StoreProduct(Product product)
    {
        if (!product.Type.Equals(Product.ProductType.INORGANIC))
        {
            storage.Add(new StoredProducts(product.Quality, product.Type.ToString()));
        }
        else
        {
            Inorganic i = product as Inorganic;
            storage.Add(new StoredProducts(product.Quality, product.Type.ToString(), i.isMetal));
        }

        product.gameObject.SetActive(false);

        storageQuality += (product.Quality / storage.Count);

        if (storage.Count >= (GameManager.Singleton.ProductCapacity) * 100)
        {
            SorterMachine.Singleton.TurnOffAll();
            GameManager.Singleton.ShowResults(storage);
        }
    }
}

[System.Serializable]
public class StoredProducts
{
    public float quality;
    public string type;
    public bool isMetal;

    public StoredProducts(float quality, string type)
    {
        this.quality = quality;
        this.type = type;
    }

    public StoredProducts(float quality, string type, bool isMetal)
    {
        this.quality = quality;
        this.type = type;
        this.isMetal = isMetal;
    }
}
