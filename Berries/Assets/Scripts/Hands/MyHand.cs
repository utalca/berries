﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyHand : MonoBehaviour {

    private SteamVR_TrackedController _controller;
    public SteamVR_Controller.Device device;

    private bool isGrabing;

    public Product current;

    private void OnEnable()
    {
        _controller = GetComponent<SteamVR_TrackedController>();
        _controller.TriggerClicked += HandleTriggerClicked;
        _controller.TriggerUnclicked += HandleTriggerReleased;
    }

    private void OnDisable()
    {
        _controller.TriggerClicked -= HandleTriggerClicked;
    }

    private void HandleTriggerClicked(object sender, ClickedEventArgs e)
    {
        Grab();
    }

    private void HandleTriggerReleased(object sender, ClickedEventArgs e)
    {
        Drop();
    }

    private void Drop()
    {
        if (isGrabing)
        {
            isGrabing = false;
            current.Deselect();

            current.Rb.velocity = device.velocity * 2;
            current.Rb.angularVelocity = device.angularVelocity;

            current = null;
        }
    }

    private void Grab()
    {
        if (!isGrabing)
        {
            if (current != null)
            {
                current.Select();
                isGrabing = true;
            }
        }
     }

    private void Update()
    {
        if (isGrabing)
        {
            if (current != null)
            {
                current.transform.GetChild(0).position = transform.position;
            }
        }

        device = SteamVR_Controller.Input((int)_controller.controllerIndex);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Product") && !isGrabing)
        {
            current = other.GetComponentInParent<Product>();
        }
    }
}
