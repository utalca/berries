﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Results : MonoBehaviour {

    public Text quality;
    public Text contaminants;
    public Text metals;
    public Text final;

    private IEnumerator Start()
    {
        float timer = 3;

        quality.text = "---";
        contaminants.text = "---";
        metals.text = "---";
        final.text = "---";

        while(timer > 0)
        {
            quality.text = Random.Range(0,9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + "%";

            timer -= Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        quality.text = (CalculateQuality() * 100).ToString("F2") + "%";

        yield return new WaitForSeconds(1);
        timer = 3;

        while (timer > 0)
        {
            contaminants.text = Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString();

            timer -= Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        contaminants.text = CalculateContaminants().ToString();

        yield return new WaitForSeconds(1);
        timer = 3;

        while (timer > 0)
        {
            metals.text = Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString();

            timer -= Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        metals.text = CalculateMetals().ToString();

        yield return new WaitForSeconds(1);
        timer = 3;

        while (timer > 0)
        {
            final.text = Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + "%";

            timer -= Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        final.text = (CalculateFinal() * 100).ToString("F2") + "%";

        yield return new WaitForSeconds(1);
        timer = 3;
    }

    float CalculateQuality()
    {
        float aux = 0;

        foreach (StoredProducts sp in GameManager.Singleton.LastResults)
        {
            aux += sp.quality;
        }

        aux /= GameManager.Singleton.LastResults.Count;

        return aux;
    }

    int CalculateContaminants()
    {
        int aux = 0;

        foreach (StoredProducts sp in GameManager.Singleton.LastResults)
        {
            if (!sp.type.Equals("FRUIT"))
                aux++;
        }

        return aux;
    }

    int CalculateMetals()
    {
        int aux = 0;

        foreach (StoredProducts sp in GameManager.Singleton.LastResults)
        {
            if (sp.isMetal)
                aux++;
        }

        return aux;
    }

    float CalculateFinal()
    {
        float aux = CalculateQuality();

        aux -= ((CalculateContaminants() - CalculateMetals()) * 5) * 0.01f;

        aux -= CalculateMetals() * 0.01f;

        aux = Mathf.Clamp(aux, 0,100);

        return aux;
    }
}
