﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        collision.transform.root.gameObject.SetActive(false);
    }
}
